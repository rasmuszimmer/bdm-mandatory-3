/**
 * Created by RasmusZimmer on 03-11-2014.
 */
var _ = require('underscore');

exports.mongoService = function(){
    var _mongoose = require('mongoose');
    var _db;
    var _locationSchema;

    this.GetDB = function () {
        return _db;
    };

    this.GetLocationSchema = function () {
        return _locationSchema;
    };

    this.Connect = function (dbUri) {
        _mongoose.connect(dbUri);
        _db = _mongoose.connection;

        return _db;
    };
    
    this.CreateSchema = function (name, fields) {

        // Create schema
        var schema = _mongoose.Schema(fields);

        // Model schema
        _mongoose.model(name, schema);
    };

    this.CreateLocationSchema = function (name, fields, index) {
        if(!index){
            throw new TypeError('Must define an index to setup a location schema');
        }

        var locationSchema = _mongoose.Schema(_.extend({
            geometry: {
                type: { type: String },
                coordinates: []
            }
        }, fields));

        // Define index
        locationSchema.index(index);

        // Return model
        return _mongoose.model(name, locationSchema);
    }
};
