/**
 * Created by RasmusZimmer on 03-11-2014.
 */
var request = require('request');

exports.fetchJSON = function(url, onSuccess){
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var collection = JSON.parse(body);
            onSuccess(collection);
        }else{
            console.error(error);
        }
    })
}