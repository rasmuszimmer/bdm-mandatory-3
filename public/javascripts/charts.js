/**
 * Created by RasmusZimmer on 10-11-2014.
 */
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
    console.log(data);
    var tableData1 = google.visualization.arrayToDataTable(data.pre2000);

    var options1 = {
        title: 'Precipitation pre-2000'
    };

    var chart1 = new google.visualization.LineChart(document.getElementById('chart1'));
    chart1.draw(tableData1, options1);

    var tableData2 = google.visualization.arrayToDataTable(data.twoK2K5);

    var options2 = {
        title: 'Precipitation 2000-2005'
    };

    var chart2 = new google.visualization.LineChart(document.getElementById('chart2'));
    chart2.draw(tableData2, options2);

    var tableData3 = google.visualization.arrayToDataTable(data.post2005);

    var options3 = {
        title: 'Precipitation post-2005'
    };

    var chart3 = new google.visualization.LineChart(document.getElementById('chart3'));
    chart3.draw(tableData3, options3);

}