var express = require('express');
var router = express.Router();
var _ = require('underscore');
var LineByLineReader = require('line-by-line');
var gcLoader = require('google_charts/gc-loader.js');
var dataFetcher = require('../data_loaders/data-fetcher');
var mongoService = require('../dal/mongodb-data-access').mongoService;

// Initialize mongoDB service
var service = new mongoService();

// Setup connection
var defaultUri = 'mongodb://localhost/test';
var db = service.Connect(defaultUri);

// Schemas
var ChargingStation;
var Bench;
var mongoData = {};
// Wire up handlers
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log("Connected to MongoDB");


    // Create or retrieve the schema and construct the model
        ChargingStation = service.CreateLocationSchema('electricalcarchargingstations', {
        id: {
            type: String,
            unique: true
        },
        street: String,
        streetNo: String
    }, { geometry: '2dsphere' });

        Bench = service.CreateLocationSchema('benches', {
        id: {
            type: String,
            unique: true
        },
        street: String,
        streetNo: String
    }, { geometry: '2dsphere' });

// Fetch the JSON from Copenhagen Data
//      Power stations
    var chargingStationsUrl = 'http://wfs-kbhkort.kk.dk/k101/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=k101:elbil_ladestander&outputFormat=json&SRSNAME=EPSG:4326'
//    Benches
    var benchesUrl = 'http://wfs-kbhkort.kk.dk/k101/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=k101:baenk&outputFormat=json&SRSNAME=EPSG:4326';

    var ituPoint = {
        type: "Point",
        coordinates: [12.591548, 55.659765]
    };


    dataFetcher.fetchJSON(chargingStationsUrl, function onSuccess(data){
        populateDB(ChargingStation, data, function onSuccess() {
            ChargingStation.where('geometry').near({
                center: ituPoint,
                maxDistance: 5000
            }).exec(function(err, results){
                mongoData.closestCharging = 'Closest charging station: ' + results[0].street + ' ' + results[0].streetNo;
                mongoData.chargingWithin5km = 'Charging stations within 5km: ' + results.length;
            });
        });
    });
    dataFetcher.fetchJSON(benchesUrl, function onSuccess(data){
        populateDB(Bench, data, function onSuccess() {
            Bench.where('geometry').near({
                center: ituPoint,
                maxDistance: 5000
            }).exec(function(err, results){
                mongoData.benchesWithin5km = 'Benches within 5km: ' + results.length;
            });
        });
    });
});

function populateDB(Model, data, callback) {
// Map the raw data onto the model
    var modelInstances = _.map(data.features, function(feature){
        return new Model({
            geometry: feature.geometry,
            id: feature.id,
            street: feature.properties.vejnavn,
            streetNo: feature.properties.husnr
        })
    });

    // "Drop" schema
    Model.remove(function onDone(){
        // "Bulk" insert from dataset. (This is a poor method but mongoose doesn't support bulk insert
        // and I cba to switch back to native mongoDB.
        var res = [];
        _.each(modelInstances, function(instance){
            instance.save(function (err) {
                if (err) return console.error(err);
                res.push(err);
                if (res.length === modelInstances.length) {
                    // Here we must be done
                    console.log("If no error has been thrown so far all " +
                        "{0} records have been saved".format(modelInstances.length));
                    callback();
                }
            });
        });
    });
}

var _this = this;
this.Observation = function (rawDate, time, precipitation, status, id) {
    this.addDate = function (date, time) {
        var year = date.substr(0, 4);
        var month = date.substr(4, 2);
        var day = date.substr(6, 2);
        return {year: year, month: month, day: day, time: time};
    }

    this.date = this.addDate(rawDate, time);
    // Correct precipitation values on first obs for the year that are not 0
    this.precipitation = this.date.day === '01' && this.date.month === '01' && this.date.time === '0'
        ? 0
        : precipitation;
    this.status = status;
    this.id = id;
}
/* GET home page. */
router.get('/', function(req, res) {

    var lineIndex = -1;
    var lines = [];
    var data = {
        observations:[]
    };
    var groupedMonthsAndYears;
    var groupedMonths;
    var lr = new LineByLineReader('data.txt');

    lr.on('error', function (err) {
        // 'err' contains error object
        console.error(err);
    });

    lr.on('line', function (line) {
        lineIndex++;
        // Acts weirdly, so pause
        lr.pause();
        // 'line' contains the current line without the trailing newline character.
        //
        lines.push(line);
        lr.resume();
    });

    lr.on('end', function () {
        // All lines are read, file is closed now.
        // Remove first line since we know that is the columns line
        lines.splice(0,1);
        data.observations = _.compact(_.map(lines, function (line, i) {
            var values = line.split("\t");
            var rawDate = values[0];
            var time = values[1];
            var precip = values[2];
            var status = values[3];
            var id = values[4];

            // Disregard observation for not OK status or after 2007 where status is left out
            if(status !== 'OK' && status !== ""){
                return null;
            }

            return new _this.Observation(rawDate, time, precip, status, id);
        }));

        // Monthly/yearly grouping
        groupedMonthsAndYears = _.groupBy(data.observations, function (obs) {
            return obs.date.year + "/" + obs.date.month
        });
        _.each(_.keys(groupedMonthsAndYears), function (key) {
            var observations = groupedMonthsAndYears[key];
            var sortedObservations = _.sortBy(observations, function (obs) {
                return Number(obs.id);
            });
            
            var lastObs = _.last(sortedObservations).precipitation;
            var lastObsParsed = parseFloat(lastObs);
            var firstObs = _.first(sortedObservations).precipitation;
            var firstObsParsed = parseFloat(firstObs);
            var monthlyTotalPrecip = lastObs - firstObs;
            groupedMonthsAndYears[key] = {
                label: key,
                monthlyTotal: monthlyTotalPrecip
            }
        });

        var groupedByYears = _.groupBy(groupedMonthsAndYears, function (group) {
            return group.label.split("/")[0];
        });
        var googleChartFormatLineCharts = {
            pre2000:[
                ['Month'],['Jan'],['Feb'],['Mar'],['Apr'],['May'],['Jun'],
                ['Jul'],['Aug'],['Sep'],['Oct'],['Nov'],['Dec']
            ],
            twoK2K5:[
                ['Month'],['Jan'],['Feb'],['Mar'],['Apr'],['May'],['Jun'],
                ['Jul'],['Aug'],['Sep'],['Oct'],['Nov'],['Dec']
            ],
            post2005:[
                ['Month'],['Jan'],['Feb'],['Mar'],['Apr'],['May'],['Jun'],
                ['Jul'],['Aug'],['Sep'],['Oct'],['Nov'],['Dec']
            ]};


        gcLoader.loadGC(googleChartFormatLineCharts.pre2000, _.filter(_.keys(groupedByYears), function(key){
            return Number(key)< 2000;
        }), groupedByYears)
        gcLoader.loadGC(googleChartFormatLineCharts.twoK2K5, _.filter(_.keys(groupedByYears), function(key){
            return Number(key) >= 2000 && Number(key) <= 2005;
        }), groupedByYears)
        gcLoader.loadGC(googleChartFormatLineCharts.post2005, _.filter(_.keys(groupedByYears), function(key){
            return Number(key)> 2005;
        }), groupedByYears)
        res.render('index', {
            title: 'BDM-Mandatory 3',
            googleChartFormatLineChartData: JSON.stringify(googleChartFormatLineCharts),
            mongoData: mongoData
        });
    });
});

router.get('/getanswers/:location', function(req, res, next) {


});
module.exports = router;
